import os
import sys
from datetime import datetime

project_path = sys.argv[1]

timestamp = datetime.strftime(datetime.now(), "%y%m%d_%H%M")

os.rename(project_path + "/fvf/Debug/bin/foxi.bin",
project_path + "/fvf/Debug/bin/foxi_" + timestamp + ".bin")
