/*
 * uart_util.h
 *
 *  Created on: May 1, 2020
 *      Author: ssdmt@mail.ru
 */

#ifndef __UART_UTIL_H
#define __UART_UTIL_H

enum uart_cmd_e {
	UART_CMD_NONE = 0,
	UART_CMD_IR,		// 1
	UART_CMD_IRIS,		// 2
	UART_CMD_FOCUS,		// 3
	UART_CMD_GPS,		// 4
	UART_CMD_GSENS,		// 5
	UART_CMD_TEMP,		// 6
	UART_CMD_REBOOT,	// 7
	UART_CMD_FAN,		// 8
	UART_CMD_HEAT,		// 9
	UART_CMD_BATT,		// 10
	UART_CMD_IND_IRIS,	// 11
	UART_CMD_VER,		// 12
};

enum uart_phase_e {
	UART_CMD_ASTERISK = 1,
	UART_CMD_PHASE,
	UART_ARG_PHASE,
};

typedef enum uart_device {
	UART_DEV_GPS,
	UART_DEV_JETSON,
	UART_DEV_INSTANCES, // total
} uart_device_t;

#endif /* __UART_UTIL_H */
