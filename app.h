/*
 * devices.h
 *
 *  Created on: May 4, 2020
 *      Author: ssdmt@mail.ru
 */

#ifndef __APP_H_
#define __APP_H_


#define APP_SET(x, y)	((x) |= (y))
#define APP_RESET(x, y)	((x) &= (~(y)))
#define APP_TEST(x, y)	((x) & (y))

typedef struct app_state_s {
	u8 motors;
#define APP_IRIS_MOTOR_OPEN		(0x00)
#define APP_IRIS_MOTOR_CLOSE	(0x01)
#define APP_FOCUS_MOTOR_LEFT	(0x02)
#define APP_FOCUS_MOTOR_RIGHT	(0x04)
#define STATE_MOTOR_FOCUS_LOCK	(0x08)
	u8 uart;
#define APP_UART_PHASE_CMD		(0x00)
#define APP_UART_PHASE_ARG1		(0x01)
#define APP_UART_PHASE_ARG2		(0x02)
#define APP_UART_PHASE_ARG3		(0x04)
#define APP_UART_PHASE_ARG4		(0x08)
#define APP_UART_PHASE_ARG5		(0x0f)

} app_state_t;

extern app_state_t state;

#endif // __APP_H_
