/*
 * ds18b20.c
 *
 *  Created on: Sep 13, 2020
 *      Author: ssdmt@mail.ru
 */

#include "ds18b20.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"

#define DELAY_RESET           500
#define DELAY_WRITE_0         60
#define DELAY_WRITE_0_PAUSE   10
#define DELAY_WRITE_1         10
#define DELAY_WRITE_1_PAUSE   60
#define DELAY_READ_SLOT       10
#define DELAY_BUS_RELAX       10
#define DELAY_READ_PAUSE      50
#define DELAY_T_CONVERT       760000
#define DELAY_PROTECTION      5

typedef enum {
	SKIP_ROM = 0xCC,
	CONVERT_T = 0x44,
	READ_SCRATCHPAD = 0xBE,
	WRITE_SCRATCHPAD = 0x4E,
	TH_REGISTER = 0x4B,
	TL_REGISTER = 0x46,
} COMMANDS;

uint32_t DELAY_WAIT_CONVERT = DELAY_T_CONVERT;

static GPIO_InitTypeDef GPIO_InitStruct;

void init_ds18b20(DS18B20_Resolution resolution)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_1;
	GPIO_Init(GPIOC, &GPIO_InitStruct);

//	GPIOC->CRL &= ~GPIO_CRL_MODE1_0; // 0  '10' - output mode 2MHz
//	GPIOC->CRL |=  GPIO_CRL_MODE1_1; // 1
//	GPIOC->CRL |=  GPIO_CRL_CNF1_0;  // 1  '01' - open-drain
//	GPIOC->CRL &= ~GPIO_CRL_CNF1_1;  // 0

	setResolution(resolution);

	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
}

void reset()
{
//	GPIOC->ODR &= ~GPIO_ODR_ODR1; // <- 0
	GPIO_ResetBits(GPIOC, GPIO_Pin_1);
	delay_ds18b20(DELAY_RESET);
//	GPIOC->ODR |= GPIO_ODR_ODR1; // <- 1
	GPIO_SetBits(GPIOC, GPIO_Pin_1);
	delay_ds18b20(DELAY_RESET);
}

uint8_t getDevider(DS18B20_Resolution resolution)
{
	uint8_t devider;
	switch (resolution) {
	case DS18B20_Resolution_9_bit:
		devider = 8;
		break;
	case DS18B20_Resolution_10_bit:
		devider = 4;
		break;
	case DS18B20_Resolution_11_bit:
		devider = 2;
		break;
	case DS18B20_Resolution_12_bit:
	default:
		devider = 1;
		break;
	}

	return devider;
}

void writeBit(uint8_t bit)
{
	__disable_irq();
//	GPIOC->ODR &= ~GPIO_ODR_ODR1; // <- 0
	GPIO_ResetBits(GPIOC, GPIO_Pin_1);
	delay_ds18b20(bit ? DELAY_WRITE_1 : DELAY_WRITE_0);
//	GPIOC->ODR |= GPIO_ODR_ODR1;  // <- 1
	GPIO_SetBits(GPIOC, GPIO_Pin_1);
	delay_ds18b20(bit ? DELAY_WRITE_1_PAUSE : DELAY_WRITE_0_PAUSE);
	__enable_irq();
}

void writeByte(uint8_t data)
{
	for (uint8_t i = 0; i < 8; i++) {
		writeBit(data >> i & 1);
		delay_ds18b20(DELAY_PROTECTION);
	}
}

uint8_t readBit()
{
	uint8_t bit = 0;

	__disable_irq();

//	GPIOC->ODR &= ~GPIO_ODR_ODR1;
	GPIO_ResetBits(GPIOC, GPIO_Pin_1);
	delay_ds18b20(DELAY_READ_SLOT);
//	GPIOC->ODR |= GPIO_ODR_ODR1;
	GPIO_SetBits(GPIOC, GPIO_Pin_1);

	// switch to INPUT
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_1;
	GPIO_Init(GPIOC, &GPIO_InitStruct);

//	GPIOC->CRL &= ~GPIO_CRL_MODE1_0; // 0 '00' - input mode
//	GPIOC->CRL &= ~GPIO_CRL_MODE1_1; // 0
//	GPIOC->CRL &= ~GPIO_CRL_CNF1_0;	 // 0 '00' - floating input
//	GPIOC->CRL &= ~GPIO_CRL_CNF1_1;  // 0

	delay_ds18b20(DELAY_BUS_RELAX);
//	bit = (GPIOC->IDR & GPIO_IDR_IDR1 ? 1 : 0);
	bit = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_1);
	delay_ds18b20(DELAY_READ_PAUSE);

	// switch to OUTPUT
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_1;
	GPIO_Init(GPIOC, &GPIO_InitStruct);

//	GPIOC->CRL &= ~GPIO_CRL_MODE1_0;  	// 0  '10' - output mode 2MHz
//	GPIOC->CRL |=  GPIO_CRL_MODE1_1;	// 1
//	GPIOC->CRL |=  GPIO_CRL_CNF1_0;		// 1  '01' - open-drain
//	GPIOC->CRL &= ~GPIO_CRL_CNF1_1;     // 0

	__enable_irq();
	return bit;
}

u16 readTemperature()
{
	u16 data = 0;
//	x = data;
//	if((x >> 15) == 1) {
//		x = x-1; x = ~x;
//		return x / -16.0;
//	} else
//		return x / 16.0;
	return data;
}

u16 ds18b20_getTemperature()
{
	reset();
	writeByte(SKIP_ROM);
	writeByte(CONVERT_T);
	delay_ds18b20(DELAY_WAIT_CONVERT);

	reset();
	writeByte(SKIP_ROM);
	writeByte(READ_SCRATCHPAD);

	uint16_t data = 0;
	for (uint8_t i = 0; i < 16; i++)
		data += (uint16_t) readBit() << i;
	// return data >> 4;
	return data;
}

void setResolution(DS18B20_Resolution resolution)
{
	reset();
	writeByte(SKIP_ROM);
	writeByte(WRITE_SCRATCHPAD);
	writeByte(TH_REGISTER);
	writeByte(TL_REGISTER);
	writeByte(resolution);
	DELAY_WAIT_CONVERT = DELAY_T_CONVERT / getDevider(resolution);
}

void delay_ds18b20(volatile uint32_t us)
{

	us *= 72;
	DWT->CYCCNT = 0;
	while ( DWT->CYCCNT < us);

//	volatile uint32_t counter = 8 * us;
//	while(counter--);
}
