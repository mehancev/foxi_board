#ifndef __EEPR_H
#define __EEPR_H



#include "stm32f10x_gpio.h"
#include "stm32f10x_i2c.h"
#include "stm32f10x_rcc.h"
#include "Delay_lib_v1.h"


#define EEPROM_HW_ADDRESS 0xA0
#define I2C_EE  I2C1


void I2C_Configuration(void);
void I2C_EE_ByteWrite(uint8_t val, uint16_t WriteAddr);
uint8_t I2C_EE_ByteRead(uint16_t ReadAddr);



#endif
