/*
 * devices.h
 *
 *  Created on: May 4, 2020
 *      Author: ssdmt@mail.ru
 */

#ifndef __DEVICE_H_
#define __DEVICE_H_

enum ir {
	IR_POWER = 1
};
enum iris {
	IRIS_OPEN = 1,
	IRIS_CLOSE,
	IRIS_INDEX,
	IRIS_CALIBRATE
};
enum focus {
	FOCUS_LEFT_1 = 1, // 1  left  B  60
	FOCUS_LEFT_2,	  // 2  left  M  15
	FOCUS_LEFT_3,     // 3  left  L  4
	FOCUS_RIGHT_1,    // 4  right B  60
	FOCUS_RIGHT_2,    // 5  right M  15
	FOCUS_RIGHT_3,    // 6  right L  4
	FOCUS_CALIBRATE
};
enum gps {
	GPS_COORDS = 1,
	GPS_TIMESTAMP
};
enum fan {
	FAN_OFF = 0,
	FAN_ON = 1
};
enum heat {
	HEAT_OFF = 0,
	HEAT_ON = 1
};

enum reset {
	RESET_OFF = 0,
	RESET_ON = 1
};

enum batt {
	BATT_VOLTAGE = 1,
	BATT_CURRENT = 2
};


#endif /* __DEVICE_H */
