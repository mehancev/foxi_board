/*
 * main.c
 *
 *  Created on: May 1, 2020
 *      Author: ssdmt@mail.ru
 */

#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_adc.h"
#include "stm32f10x_exti.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_iwdg.h"
#include "stm32f10x_dma.h"
#include "stm32f10x_flash.h"
#include "eeprom_v2.h"
#include "misc.h"

#include "uart_util.h"
#include "device.h"
#include "app.h"
#include "ds18b20.h"

#include <string.h>

#define UART_INIT_TRESHOLD	10	// 160

#define  IN1_1		GPIO_Pin_11 //PB
#define  EN1_1		GPIO_Pin_12 //PB
#define  IN2_1		GPIO_Pin_13 //PB
#define  EN2_1		GPIO_Pin_14 //PB

#define  IN1_2		GPIO_Pin_15 //PB
#define  EN1_2		GPIO_Pin_6  //PC
#define  IN2_2		GPIO_Pin_7  //PC
#define  EN2_2		GPIO_Pin_8  //PC

#define  STP_V_LOW	GPIO_Pin_15 //PA

#define  LED_ 		GPIO_Pin_8  //PB
#define  OUT1 		GPIO_Pin_12 //PA
#define  OUT2		GPIO_Pin_11 //PA
#define	 OUT3		GPIO_Pin_7  //PA
#define  RES_JET	GPIO_Pin_0  //PA

#define TEST_STEP_COUNT		20
#define TEST_STEP_SPEED		20
#define PWM_W				147
#define RX_BUFFER_SIZE		60

void gpioInit();
void timersInit();
void adcInit();
void IntConf();
void USART_Configuration();
void DMA_UART_Configuration();
void DMA_UART_Send_Data(uint8_t *data, uint16_t len);
void DMA_UART_Stop();

void DMA_UART2_Configuration();
void DMA_UART2_Stop();
void DMA_UART2_Send_Data(uint8_t *data, uint16_t len);

u8 IRIS_open();
u8 IRIS_close();
u8 FOCUS_left();
u8 FOCUS_right();
u8 motors_idle();

u8 FLASH_init();

uint16_t getAdcValue(uint8_t chanel, uint8_t ADC_SampleTime);
void PWM_Pulse(uint16_t value);
void uart_rx(void);
void uart_rx2(void);
void uart2_rx(void);
int get_char(void);
int get_char2(void);
void TestSendData();
void TestSendData2();
void Calib_IRIS(void);
uint16_t BAT_SCAN(void);
void setEEIrisPos(uint8_t pos);
uint8_t getEEIrisPos();
void get_gps_coords(uint8_t *gps, uint8_t *_lat, uint8_t *_lng);
void get_gps_timestamp(uint8_t *gps,  uint8_t *_date, uint8_t *_time);

// motors
uint8_t flag_led=0;
uint8_t flag_ext0=0;

uint8_t l_phase=0;
uint8_t r_phase=0;
uint8_t l_phase2=0;
uint8_t r_phase2=0;

uint16_t l_step=0;
uint16_t r_step=0;
uint16_t l_step2=0;
uint16_t r_step2=0;

uint16_t pause=0;
uint16_t pause2=0;
uint16_t step_speed=10;
uint16_t step_speed2=10;

uint8_t pause_calib=0;

// uart, dma_uart etc
uint8_t tx_dma_flag=0;
uint16_t rx_rd_index=0, tx_end=0;
uint8_t rx_buffer[RX_BUFFER_SIZE];
uint8_t tx_buffer[(RX_BUFFER_SIZE*2)];

uint8_t tx_dma_flag2=0;
uint16_t rx_rd_index2=0,tx_end2=0;
uint8_t rx_buffer2[RX_BUFFER_SIZE];
uint8_t tx_buffer2[(RX_BUFFER_SIZE*2)];

uint16_t systick=0;

volatile u8 uart_start_count = 0;
volatile u8 uart_init_first = 0;

uint8_t pos_iris=3;

uint16_t vbat=0;
uint8_t tmpu[10];

char *gps_RMC1 = "$PMTK314,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*2A\r\n";
char *gps_RMC2 = "$PMTK414*33\r\n";

static uint8_t lat[13] = {'0','0','0','0','.','0','0','0','0','0','0','\r','\n'};
static uint8_t lng[14] = {'0','0','0','0','0','.','0','0','0','0','0','0','\r','\n'};
static uint8_t date[9] = {'2','0','0','1','0','1','\r','\n','\0'};
static uint8_t time[9] = {'1','1','1','1','1','1','\r','\n','\0'};

uint8_t *version = "210309_1325\r\n\0";  // 13 chars

app_state_t app_state;

void WDG_init(void) {
	// �������� LSI
	RCC_LSICmd(ENABLE);
	while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET);
	// ����������� ������ � ��������� IWDG
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	// ������������� ������������
	IWDG_SetPrescaler(IWDG_Prescaler_256);
	// �������� ��� ������������
	IWDG_SetReload(600);
	// ������������ ��������
	IWDG_ReloadCounter();
	// LSI ������ ���� �������
	IWDG_Enable();
}

void USART1_IRQHandler(void)
{
	// interrupt by sending complete
	if(USART_GetITStatus(USART1, USART_IT_TC)!=RESET)
	{
		USART_ClearITPendingBit(USART1, USART_IT_TC); // clear interrupt flag
		tx_end=1;
	}
}

void USART2_IRQHandler(void)
{
  if(USART_GetITStatus(USART2, USART_IT_TC)!=RESET) //TX
  {
	  USART_ClearITPendingBit(USART2, USART_IT_TC);
	  tx_end2=1;
  }
}

void DMA1_Channel4_IRQHandler(void)
{
	//USART_ClearITPendingBit(USART1, USART_IT_TC);
	DMA1->IFCR |= DMA_ISR_TCIF4;
	tx_dma_flag=1;
}

void DMA1_Channel7_IRQHandler(void)
{
	//USART_ClearITPendingBit(USART1, USART_IT_TC);
	DMA1->IFCR |= DMA_ISR_TCIF7;
	tx_dma_flag2=1;
}

void SysTick_Handler()
{
	if(pause>0)pause--;
	if(pause2>0)pause2--;
	systick++;
}

void EXTI0_IRQHandler(void)
{
	//flag_ext0=GPIO_ReadInputDataBit(GPIOB,BASLER_STATUS);
	EXTI_ClearFlag(EXTI_Line0);
}

void DMA_Clear() {
	if(tx_dma_flag!=0 && tx_end!=0) {
		DMA_UART_Stop(); tx_dma_flag=0;
	}
	if(tx_dma_flag==0) tx_end=0;
	if(tx_dma_flag2!=0 && tx_end2!=0) {
		DMA_UART2_Stop(); tx_dma_flag2=0;
	}
	if(tx_dma_flag2==0) tx_end2=0;
}

char *my_ftoa(float val, char *str)
{
	char *cp; cp=str;
	int v, v0, rest, rest0;
	char c;
	if(val < 0){ // cp=0
		val = -val; //cp[0 ][1 ][2][3][4][5][6][7][8][9]
		*cp++ = '-'; // [0: -] cp=1
	}
	v0 = (int)val; v=v0;
	//rest0=(int)((val-(int)val)*100000000); rest = rest0;
	rest0=(int)((val-(int)val)*10000); rest = rest0;
	do {
		v /= 10;
		cp++;
	} while(v != 0);
	do {
		rest /= 10;
		cp++;
	} while(rest != 0);
	cp++; //wegen ','
	*cp-- = 0;
	do {
		c = rest0 % 10;
		rest0 /= 10;
		c += '0';
		*cp-- = c;
	} while(rest0 != 0);
	*cp-- = ',';
	do {
		c = v0 % 10;
		v0 /= 10;
		c += '0';
		*cp-- = c;
	} while(v0 != 0);
	return cp;
}

char* my_utoa(unsigned val, char *str)
{
	//static char buffer[10];
	char* cp = str;
	unsigned v;
	char c;
	v = val;
	do {
		v /= 10;
		cp++;
	} while(v != 0);
	*cp-- = 0;
	do {
		c = val % 10;
		val /= 10;
		c += '0';
		*cp-- = c;
	} while(val != 0);
	return cp;
}

uint16_t slen(const char* s) {
	uint16_t i;
	for (i = 0; s[i] != 0; i++);
	return i;//s[0] not 0 then i=1;
}

void add_txt(char* out, char* in) {
	while (*out != 0) out++;
	while (*in != 0) {
		*out++ = *in++;
	}
	*out = 0;
}

int main(void)
{
//	uint8_t alarm_buf[4] = {'*',':','3','4'}; // test
	SystemInit();

	//FLASH_init();
	gpioInit();
	timersInit();
	adcInit();
	I2C_Configuration();

//	IntConf();

	SysTick_Config(SystemCoreClock/1000); // every 1ms

	WDG_init();

	GPIO_SetBits(GPIOB,STP_V_LOW); // 3.3V - '1'; 5V - '0'

	GPIO_SetBits(GPIOA, OUT1);   // delayed power ON

	GPIO_ResetBits(GPIOA, OUT2); // OFF
	GPIO_ResetBits(GPIOA, OUT3); // OFF

	pos_iris = getEEIrisPos();
	if((pos_iris>12)||(pos_iris<0)) {
		pos_iris=4;
		setEEIrisPos(pos_iris);
	}

	l_step2=95;
	pause_calib=1;

	uart_start_count=UART_INIT_TRESHOLD;

//	u16 T;
//	char txt[32] = {0};

	while(1)
	{
		IWDG_ReloadCounter();

		IRIS_open();
		IRIS_close();
		FOCUS_left();
		FOCUS_right();
		motors_idle();

		if(!uart_start_count)
			DMA_Clear();

		if(systick > 500) // every 0.5 sec
		{
			systick=0;
			if(flag_led==0) {
				flag_led=1;
				GPIO_SetBits(GPIOB,LED_);
			} else {
				GPIO_ResetBits(GPIOB,LED_);
				flag_led=0;

//				if(!uart_start_count) {
//					T = ds18b20_getTemperature();
//					my_ftoa(T, txt);
//					my_utoa(T, txt);
//					add_txt(txt, "\n");
//					DMA_UART_Send_Data((uint8_t*)txt, slen(txt));
//					DMA_UART_Send_Data((u8*)&T, 2);

//					DMA_UART_Send_Data(&T, 2);
//					uint8_t bit = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_1);
//					if(!bit)
//						DMA_UART_Send_Data(alarm_buf, 1);
//				}
// debug temperature
//				if(!uart_start_count) {
//					AD_value = getAdcValue(ADC_Channel_16, ADC_SampleTime_239Cycles5);
//					T = ((1.43-(AD_value*0.00080566))/0.0043)+25;
//
//					tmp = T & 0x00ff;
//					DMA_UART_Send_Data((u8*)&(tmp), 1);
//				}
			}
			if(pause_calib!=0)
				pause_calib++;
			if(pause_calib>=5) {
				pause_calib=0;
				Calib_IRIS();
			}

			if(uart_start_count>0) // stage 0
				uart_start_count--;

			if(uart_start_count==1) { // stage 2
				DMA_Clear();
				DMA_UART2_Send_Data((uint8_t*)gps_RMC1, 51); //  init gps sensor
				DMA_Clear();
				DMA_UART2_Send_Data((uint8_t*)gps_RMC2, 13);
				for(int ii=0; ii < 65535; ii++);

				init_ds18b20(DS18B20_Resolution_9_bit);
			}

			if(uart_start_count==2) { // stage 3
				USART_Configuration();
				DMA_UART_Configuration();
				DMA_UART2_Configuration();

				GPIO_SetBits(GPIOA, OUT2); // delayed power ON
				GPIO_SetBits(GPIOA, OUT3);
			}
//			if(!uart_start_count)
//				TestSendData();

		} // if(systick > 500) { ...

		if(!uart_start_count) {
			uart_rx();
			uart2_rx();
		}
	}
}

u8 FLASH_init()
{
	FLASH->KEYR = 0x45670123;
	FLASH->KEYR = 0xCDEF89AB;

	return 0;
}

u8 IRIS_open()
{
	if(r_step2!=0)
	{
		if(r_phase2==0) {
			GPIO_ResetBits(GPIOB,IN1_2); GPIO_ResetBits(GPIOC,IN2_2); GPIO_SetBits(GPIOC,EN1_2); GPIO_ResetBits(GPIOC,EN2_2);
			pause2=step_speed2; r_step2--; r_phase2++;
		}
		if(pause2!=0 && r_phase2==1)return 0;

		if(r_phase2==1) {
			GPIO_ResetBits(GPIOB,IN1_2); GPIO_ResetBits(GPIOC,IN2_2); GPIO_ResetBits(GPIOC,EN1_2); GPIO_SetBits(GPIOC,EN2_2);
			pause2=step_speed2; r_step2--; r_phase2++;
		}
		if(pause2!=0 && r_phase2==2)return 0;

		if(r_phase2==2) {
			GPIO_SetBits(GPIOB,IN1_2); GPIO_SetBits(GPIOC,IN2_2); GPIO_SetBits(GPIOC,EN1_2); GPIO_ResetBits(GPIOC,EN2_2);
			pause2=step_speed2;	r_step2--; r_phase2++;
		}
		if(pause2!=0 && r_phase2==3)return 0;

		if(r_phase2==3) {
			GPIO_SetBits(GPIOB,IN1_2); GPIO_SetBits(GPIOC,IN2_2); GPIO_ResetBits(GPIOC,EN1_2); GPIO_SetBits(GPIOC,EN2_2);
			pause2=step_speed2; r_step2--; r_phase2++;
		}
		if(pause2!=0 && r_phase2==4)return 0;

		r_phase2=0;
	}
	return 0;
}

u8 IRIS_close()
{
	if(l_step2!=0)
	{
		if(l_phase2==0) {
			GPIO_SetBits(GPIOB,IN1_2); GPIO_SetBits(GPIOC,IN2_2); GPIO_ResetBits(GPIOC,EN1_2); GPIO_SetBits(GPIOC,EN2_2);
			pause2=step_speed2; l_step2--; l_phase2++;
		}
		if(pause2!=0 && l_phase2==1)return 0;

		if(l_phase2==1) {
			GPIO_SetBits(GPIOB,IN1_2); GPIO_SetBits(GPIOC,IN2_2); GPIO_SetBits(GPIOC,EN1_2); GPIO_ResetBits(GPIOC,EN2_2);
			pause2=step_speed2; l_step2--; l_phase2++;
		}
		if(pause2!=0 && l_phase2==2)return 0;

		if(l_phase2==2) {
			GPIO_ResetBits(GPIOB,IN1_2); GPIO_ResetBits(GPIOC,IN2_2); GPIO_ResetBits(GPIOC,EN1_2); GPIO_SetBits(GPIOC,EN2_2);
			pause2=step_speed2; l_step2--; l_phase2++;
		}
		if(pause2!=0 && l_phase2==3)return 0;

		if(l_phase2==3) {
			GPIO_ResetBits(GPIOB,IN1_2); GPIO_ResetBits(GPIOC,IN2_2); GPIO_SetBits(GPIOC,EN1_2); GPIO_ResetBits(GPIOC,EN2_2);
			pause2=step_speed2; l_step2--; l_phase2++;
		}
		if(pause2!=0 && l_phase2==4)return 0;

		l_phase2=0;
	}

	return 0;
}

u8 FOCUS_left()
{
	if(r_step!=0)
	{
		if(r_phase==0) {
			GPIO_ResetBits(GPIOB,IN1_1); GPIO_ResetBits(GPIOB,IN2_1); GPIO_SetBits(GPIOB,EN1_1); GPIO_ResetBits(GPIOB,EN2_1);
			pause=step_speed; r_step--; r_phase++;
		}
		if(pause!=0 && r_phase==1)return 0;

		if(r_phase==1) {
			GPIO_ResetBits(GPIOB,IN1_1); GPIO_ResetBits(GPIOB,IN2_1); GPIO_ResetBits(GPIOB,EN1_1); GPIO_SetBits(GPIOB,EN2_1);
			pause=step_speed; r_step--; r_phase++;
		}
		if(pause!=0 && r_phase==2)return 0;

		if(r_phase==2) {
			GPIO_SetBits(GPIOB,IN1_1); GPIO_SetBits(GPIOB,IN2_1); GPIO_SetBits(GPIOB,EN1_1); GPIO_ResetBits(GPIOB,EN2_1);
			pause=step_speed; r_step--; r_phase++;
		}
		if(pause!=0 && r_phase==3)return 0;

		if(r_phase==3) {
			GPIO_SetBits(GPIOB,IN1_1); GPIO_SetBits(GPIOB,IN2_1); GPIO_ResetBits(GPIOB,EN1_1); GPIO_SetBits(GPIOB,EN2_1);
			pause=step_speed; r_step--; r_phase++;
		}
		if(pause!=0 && r_phase==4)return 0;
		r_phase=0;
	}
	return 0;
}

u8 FOCUS_right()
{
	if(l_step!=0)
	{
		if(l_phase==0) {
			GPIO_SetBits(GPIOB,IN1_1); GPIO_SetBits(GPIOB,IN2_1); GPIO_ResetBits(GPIOB,EN1_1); GPIO_SetBits(GPIOB,EN2_1);
			pause=step_speed; l_step--; l_phase++;
		}
		if(pause!=0 && l_phase==1)return 0;

		if(l_phase==1) {
			GPIO_SetBits(GPIOB,IN1_1); GPIO_SetBits(GPIOB,IN2_1); GPIO_SetBits(GPIOB,EN1_1); GPIO_ResetBits(GPIOB,EN2_1);
			pause=step_speed; l_step--;	l_phase++;
		}
		if(pause!=0 && l_phase==2)return 0;
		if(l_phase==2) {
			GPIO_ResetBits(GPIOB,IN1_1); GPIO_ResetBits(GPIOB,IN2_1); GPIO_ResetBits(GPIOB,EN1_1); GPIO_SetBits(GPIOB,EN2_1);
			pause=step_speed; l_step--; l_phase++;
		}
		if(pause!=0 && l_phase==3)return 0;
		if(l_phase==3) {
			GPIO_ResetBits(GPIOB,IN1_1); GPIO_ResetBits(GPIOB,IN2_1); GPIO_SetBits(GPIOB,EN1_1); GPIO_ResetBits(GPIOB,EN2_1);
			pause=step_speed; l_step--; l_phase++;
		}
		if(pause!=0 && l_phase==4)return 0;
		l_phase=0;
	}
	return 0;
}

u8 motors_idle()
{ // relax ))
	if((r_step==0) && (l_step==0) && (pause==0))	{
		GPIO_ResetBits(GPIOB,EN1_1); // EN1_1=0;
		GPIO_ResetBits(GPIOB,EN2_1); // EN2_1=0;
	}
	if((r_step2==0) && (l_step2==0) && (pause2==0)) {
		GPIO_ResetBits(GPIOC,EN1_2); // EN1_2=0;
		GPIO_ResetBits(GPIOC,EN2_2); // EN2_2=0;
	}
	return 0;
}

void gpioInit()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOB |RCC_APB2Periph_GPIOA ,ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable,ENABLE);

	GPIO_InitTypeDef gpio_setup;

	gpio_setup.GPIO_Mode=GPIO_Mode_Out_PP;
	gpio_setup.GPIO_Pin=GPIO_Pin_0 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_15 | GPIO_Pin_7;
	gpio_setup.GPIO_Speed=GPIO_Speed_10MHz;
	GPIO_Init(GPIOA, &gpio_setup);


	gpio_setup.GPIO_Mode=GPIO_Mode_AF_PP;
	gpio_setup.GPIO_Pin=GPIO_Pin_8;
	gpio_setup.GPIO_Speed=GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &gpio_setup);

//	gpio_setup.GPIO_Mode=GPIO_Mode_AIN;
//	gpio_setup.GPIO_Pin=GPIO_Pin_4 | GPIO_Pin_6;
//	GPIO_Init(GPIOA, &gpio_setup);


	gpio_setup.GPIO_Mode=GPIO_Mode_Out_PP;
	gpio_setup.GPIO_Pin= GPIO_Pin_8 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	gpio_setup.GPIO_Speed=GPIO_Speed_10MHz;
	GPIO_Init(GPIOB, &gpio_setup);

	gpio_setup.GPIO_Mode=GPIO_Mode_Out_PP;
	gpio_setup.GPIO_Pin=   GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8;
	gpio_setup.GPIO_Speed=GPIO_Speed_2MHz;
	GPIO_Init(GPIOC, &gpio_setup);

	gpio_setup.GPIO_Mode=GPIO_Mode_AIN;
	gpio_setup.GPIO_Pin=GPIO_Pin_2;
	GPIO_Init(GPIOC, &gpio_setup);

//	gpio_setup.GPIO_Mode=GPIO_Mode_IN_FLOATING;
//	gpio_setup.GPIO_Pin=GPIO_Pin_1;
//	GPIO_Init(GPIOC, &gpio_setup);
}

void adcInit()
{
	RCC_ADCCLKConfig(RCC_PCLK2_Div8);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);

	ADC_InitTypeDef ADC_InitStructure;
	ADC_StructInit(&ADC_InitStructure);
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1; //
	ADC_Init(ADC1, &ADC_InitStructure);

	ADC_TempSensorVrefintCmd(ENABLE); 	//wake up temperature sensor

	ADC_Cmd(ADC1, ENABLE);

	ADC_ResetCalibration(ADC1);
	while (ADC_GetResetCalibrationStatus(ADC1));
	ADC_StartCalibration(ADC1);
	while (ADC_GetCalibrationStatus(ADC1));
}

void timersInit()
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 ,ENABLE);

	TIM_TimeBaseInitTypeDef tim_config;
	TIM_OCInitTypeDef tim_oc_config;

	tim_config.TIM_Prescaler=(uint16_t) (SystemCoreClock/4800000)-1;
	tim_config.TIM_Period=149;
	tim_config.TIM_ClockDivision=0;
	tim_config.TIM_CounterMode=TIM_CounterMode_Up;
	tim_config.TIM_RepetitionCounter=0;
	TIM_TimeBaseInit(TIM1,&tim_config);

	tim_oc_config.TIM_OCMode=TIM_OCMode_PWM1;
	tim_oc_config.TIM_OutputState=TIM_OutputState_Enable;
	//tim_oc_config.TIM_OutputNState=TIM_OutputNState_Enable;
	tim_oc_config.TIM_Pulse=PWM_W;
	tim_oc_config.TIM_OCPolarity=TIM_OCPolarity_High;
	tim_oc_config.TIM_OCNPolarity=TIM_OCNPolarity_High;
	tim_oc_config.TIM_OCIdleState=TIM_OCIdleState_Set;
	tim_oc_config.TIM_OCNIdleState=TIM_OCNIdleState_Reset;
	TIM_OC1Init(TIM1,&tim_oc_config);

	TIM_OC1PreloadConfig(TIM1,TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM1,ENABLE);

	TIM_Cmd(TIM1,ENABLE);
	TIM_CtrlPWMOutputs(TIM1,ENABLE);

//	NVIC_InitTypeDef nvic;
//	TIM_TimeBaseInitTypeDef tim_config;
//	TIM_OCInitTypeDef tim_oc_config;
//
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4 ,ENABLE);
//
//	tim_config.TIM_Prescaler=(uint16_t) (SystemCoreClock/4800000)-1;
//	tim_config.TIM_Period=149;
//	tim_config.TIM_ClockDivision=0;
//	tim_config.TIM_CounterMode=TIM_CounterMode_Up;
//	TIM_TimeBaseInit(TIM4,&tim_config);
//
//	tim_oc_config.TIM_OCMode=TIM_OCMode_PWM1;
//	tim_oc_config.TIM_OutputState=TIM_OutputState_Enable;
//	tim_oc_config.TIM_Pulse=pulse;
//	tim_oc_config.TIM_OCPolarity=TIM_OCNPolarity_High;
//	TIM_OC3Init(TIM4,&tim_oc_config);
//
//	TIM_OC3PreloadConfig(TIM4,TIM_OCPreload_Enable);
//	TIM_ARRPreloadConfig(TIM4,ENABLE);
//	TIM_Cmd(TIM4,ENABLE);
//
//	nvic.NVIC_IRQChannel=TIM2_IRQn;
//	nvic.NVIC_IRQChannelPreemptionPriority=0;
//	nvic.NVIC_IRQChannelSubPriority=0;
//	nvic.NVIC_IRQChannelCmd=ENABLE;
//	NVIC_Init(&nvic);
}

//void IntConf()
//{
//	EXTI_InitTypeDef exti;
//	NVIC_InitTypeDef nvic;
//
//	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource0);
//
//	exti.EXTI_Line=EXTI_Line0;
//	exti.EXTI_Mode=EXTI_Mode_Interrupt;
//	exti.EXTI_Trigger=EXTI_Trigger_Rising_Falling;
//	exti.EXTI_LineCmd=ENABLE;
//	EXTI_Init(&exti);
//
//	nvic.NVIC_IRQChannel=EXTI0_IRQn;
//	nvic.NVIC_IRQChannelCmd=ENABLE;
//	NVIC_Init(&nvic);
//	NVIC_EnableIRQ(EXTI0_IRQn);
//
//}

void USART_Configuration(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3 | GPIO_Pin_10;   //rx
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_9;    //tx
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

    //---------------------------------------------------

	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);
	USART_Init(USART2, &USART_InitStructure);

	USART_Cmd(USART1,ENABLE);
	USART_Cmd(USART2,ENABLE);

	USART_ITConfig(USART1, USART_IT_TC , ENABLE);
	USART_ITConfig(USART2, USART_IT_TC , ENABLE);
	//USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);
	//RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	//---------uart1---------------
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	//------uart2---------------------
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}

void DMA_UART_Configuration()
{
	DMA_InitTypeDef DMA_InitStructure;
	//---------------------TX----------------------------------------------
	DMA_DeInit(DMA1_Channel4);

	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(USART1->DR);
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) tx_buffer;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructure.DMA_BufferSize = 0;//len;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel4, &DMA_InitStructure);

	//---------------------RX--------------------------------------------------
	DMA_DeInit(DMA1_Channel5);

	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(USART1->DR);
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) rx_buffer;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_BufferSize = RX_BUFFER_SIZE;//len;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel5, &DMA_InitStructure);

	USART_DMACmd(USART1, USART_DMAReq_Tx | USART_DMAReq_Rx, ENABLE);
	DMA_Cmd(DMA1_Channel5, ENABLE);
	DMA_ITConfig(DMA1_Channel4,DMA_IT_TC,ENABLE);
	NVIC_EnableIRQ(DMA1_Channel4_IRQn);
}

void DMA_UART2_Configuration()
{
	DMA_InitTypeDef DMA_InitStructure;
	//---------------------TX----------------------------------------------
	DMA_DeInit(DMA1_Channel7);

	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(USART2->DR);
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) tx_buffer2;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStructure.DMA_BufferSize = 0;//len;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel7, &DMA_InitStructure);

	//---------------------RX--------------------------------------------------
	DMA_DeInit(DMA1_Channel6);

	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(USART2->DR);
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) rx_buffer2;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_BufferSize = RX_BUFFER_SIZE;//len;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_Low;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_Init(DMA1_Channel6, &DMA_InitStructure);

	USART_DMACmd(USART2, USART_DMAReq_Tx | USART_DMAReq_Rx, ENABLE);
	DMA_Cmd(DMA1_Channel6, ENABLE);
	DMA_ITConfig(DMA1_Channel7,DMA_IT_TC,ENABLE);
	NVIC_EnableIRQ(DMA1_Channel7_IRQn);
}

uint16_t getAdcValue(uint8_t chanel, uint8_t ADC_SampleTime)
{
	ADC_RegularChannelConfig(ADC1, chanel, 1, ADC_SampleTime/*ADC_SampleTime_13Cycles5*/);
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
	return ADC_GetConversionValue(ADC1);
}

void PWM_Pulse(uint16_t value)
{
	TIM1->CCR1=value;
}

void DMA_UART_Send_Data(uint8_t *data, uint16_t len)
{
	static uint8_t i=0;

	while(DMA_GetCurrDataCounter(DMA1_Channel4));
	DMA_Cmd(DMA1_Channel4, DISABLE);
	i=0;
	while(i!=len)
	{
		tx_buffer[i]=data[i];
		i++;
	}
	DMA_SetCurrDataCounter(DMA1_Channel4,len);
	DMA_Cmd(DMA1_Channel4, ENABLE);
}

void DMA_UART2_Send_Data(uint8_t *data, uint16_t len)
{
	static uint8_t i=0;

	while(DMA_GetCurrDataCounter(DMA1_Channel7));
	DMA_Cmd(DMA1_Channel7, DISABLE);
	i=0;
	while(i!=len)
	{
		tx_buffer2[i]=data[i];
		i++;
	}
	DMA_SetCurrDataCounter(DMA1_Channel7,len);
	DMA_Cmd(DMA1_Channel7, ENABLE);

}

void DMA_UART_Stop()
{
	DMA_Cmd(DMA1_Channel4, DISABLE);
}

void DMA_UART2_Stop()
{
	DMA_Cmd(DMA1_Channel7, DISABLE);
}

uint8_t temp_A[4] = {'A',':','3','4'};
uint8_t temp_B[4] = {'B',':','3','4'};

void uart_rx(void)
{
	static enum uart_phase_e uart_phase = UART_CMD_ASTERISK;
	static u8 cmd = 0;
	int data = 0;

	int d_index = 0; // unit to end location
	uint8_t iris_index, // current location
			ir_power;

	uint16_t AD_value;
	uint16_t T; // cpu temperature

	char txt[32] = {0};

	data = get_char();

	if ((data < 0)) return; // DMA empty
	if(data == '*') { // 42; 2A
		uart_phase = UART_CMD_PHASE;
		return;
	}

	switch(uart_phase) {
	case UART_CMD_PHASE:
		cmd = data;
		uart_phase = UART_ARG_PHASE; // next state
		break;
	case UART_ARG_PHASE: // data ~ arg1
		uart_phase = UART_CMD_PHASE; // next state (begin)
		switch(cmd) {
		case UART_CMD_IR/*1*/:
			if (data > 10) { // for the first time
				ir_power = I2C_EE_ByteRead(1);
				DMA_UART_Send_Data((uint8_t*)&ir_power, 1);
				break;
			}

			if (data<1) data=1;

			switch (data) {
			case 1: PWM_Pulse(10); break;
			case 2: PWM_Pulse(20); break;
			case 3: PWM_Pulse(30); break;
			case 4: PWM_Pulse(40); break;
			case 5: PWM_Pulse(50); break;
			case 6: PWM_Pulse(60); break;
			case 7: PWM_Pulse(70); break;
			case 8: PWM_Pulse(80); break;
			case 9: PWM_Pulse(140); break;
			}
			//DMA_UART_Send_Data((uint8_t*)&data, 1); // echo
			I2C_EE_ByteWrite(data, 1);
			break;
		case UART_CMD_IRIS/*2*/:
			if((data < 1) || (data > 12))
				break;
			iris_index = getEEIrisPos();
			d_index = data - iris_index;
			if(d_index < 0) {
				l_step2 = -8 * d_index;	// close '7' - one step
				iris_index += d_index;
				if (iris_index < 1) iris_index = 1;
				setEEIrisPos(iris_index);
			} else if (d_index > 0) {
				r_step2 = 8 * d_index;	// open '7' - one step
				iris_index += d_index;
				if (iris_index > 12) iris_index = 12;
				setEEIrisPos(iris_index);
			} else
				break;
			DMA_UART_Send_Data((uint8_t*)&data, 1);
			step_speed2=10;
			break;
		case UART_CMD_FOCUS/*3*/:  // TODO:
			if(data==FOCUS_LEFT_1) r_step=40;
			else if(data==FOCUS_LEFT_2) r_step=20;
			else if(data==FOCUS_LEFT_3) r_step=4;

			if(data==FOCUS_RIGHT_1) l_step=40;
			else if(data==FOCUS_RIGHT_2) l_step=20;
			else if(data==FOCUS_RIGHT_3) l_step=4;
			step_speed=10;
			break;
		case UART_CMD_GPS:
			if (data==GPS_COORDS) {
				DMA_UART_Send_Data(lat, 11);
				DMA_UART_Send_Data(lng, 12);
			} else if (GPS_TIMESTAMP) {
				DMA_UART_Send_Data(date, 6);
				DMA_UART_Send_Data(time, 6);
			}
			break;
		case UART_CMD_TEMP:
			// data = 0x00

			//    		 AD_value = getAdcValue(ADC_Channel_16, ADC_SampleTime_239Cycles5);
			//    		 T = ((1.43-(AD_value*0.00080566))/0.0043)+25;
			//    		 DMA_UART_Send_Data((u8*)&T, 2);
			T = ds18b20_getTemperature();
			my_ftoa(T, txt);
			//					my_utoa(T, txt);
			add_txt(txt, "\n");
			DMA_UART_Send_Data((uint8_t*)txt, slen(txt));
			//				DMA_UART_Send_Data((u8*)&T, 2);
			break;
		case UART_CMD_BATT:
			if (data==BATT_VOLTAGE) {
				vbat = BAT_SCAN();
				DMA_UART_Send_Data((u8*)&vbat, 2);
			}
			// else if (data == BATT_CURRENT) { .. }
			break;
		case UART_CMD_REBOOT:/*7*/
			//    		 if(data == RESET_ON)
			//    		    GPIO_SetBits(GPIOA, GPIO_Pin_8); // switch ON power relay
			//
			//    		 else if(data == RESET_OFF)
			//    			 GPIO_ResetBits(GPIOA, GPIO_Pin_8); // switch OFF power relay

			while(1); // wdg
			break;
		case UART_CMD_IND_IRIS/*11*/:
			iris_index = getEEIrisPos();
			DMA_UART_Send_Data(&iris_index, 1);
			break;
		case UART_CMD_VER/*12*/:
			DMA_UART_Send_Data(version, 11);
			break;
		default: break;
		}	// switch(cmd) { ...
		uart_phase = UART_CMD_ASTERISK; // next state (begin)
		break;
		default: break;
	} // switch(uart_phase) { ...
}

uint8_t flag_NMEA = 0;
uint8_t gps_data[100]={0};
uint8_t i=0;

// $GNRMC,060633.000,A,5647.787156,N,06030.703488,E,0.04,51.15,100620,,,A*
// $GPRMC,195100.089,V,,,,,0.00,0.00,010920,,,*
// $GPRMC,061042.543,V,,,,,0.00,0.00,020920,,,N
void uart2_rx(void)
{
	int data;

	data=get_char2();
	if(data<0)return; // DMA empty

	if(data=='$') {
		flag_NMEA = 1;
	}
	if(data=='*') {
		if(flag_NMEA) { // parsing NMEA
			get_gps_coords(gps_data, lat, lng);
			get_gps_timestamp(gps_data, date, time);
		}
		memset(gps_data,0x00, 100);
		flag_NMEA = 0;
		i=0;
	}
	if(flag_NMEA) { // just copy to work buffer
		gps_data[i++] = data;
		if(i>99)i=99;
	}
}

int get_char(void)
{
	if(RX_BUFFER_SIZE-DMA_GetCurrDataCounter(DMA1_Channel5)!=rx_rd_index)
	{
		int data = rx_buffer[rx_rd_index];
		if (++rx_rd_index == RX_BUFFER_SIZE) rx_rd_index=0;
		return data;
	}
	return -1;
}

int get_char2(void)
{
	if(RX_BUFFER_SIZE-DMA_GetCurrDataCounter(DMA1_Channel6)!=rx_rd_index2)
	{
		int data = rx_buffer2[rx_rd_index2];
		if (++rx_rd_index2 == RX_BUFFER_SIZE) rx_rd_index2=0;
		return data;
	}
	return -1;
}

uint8_t temp_buf[4] = {'.',':','3','4'};
void TestSendData()
{
	DMA_UART_Send_Data(temp_buf, 1);
}

void TestSendData2()
{
	DMA_UART2_Send_Data(temp_buf, 4);
}

void Calib_IRIS(void)
{
	r_step2 = (pos_iris-1) * 8;
}

uint16_t BAT_SCAN(void)
{
	// V=tmp_bat*0.0037
	return getAdcValue(ADC_Channel_12, ADC_SampleTime_13Cycles5);
}

void setEEIrisPos(uint8_t pos)
{
	I2C_EE_ByteWrite(pos, 0);
}

uint8_t getEEIrisPos()
{
	return I2C_EE_ByteRead(0);
}

void get_gps_coords(uint8_t *gps, uint8_t *_lat, uint8_t *_lng)
{
	uint8_t i=0, cnt_comma=0, cnt_lat=0, cnt_lon=0;

	while(i < strlen((const char*)gps))
	{
		if(gps[i]==',')
		{
			cnt_comma++;
			i++;

			if((cnt_comma == 2) && (gps[i] == 'V')) //+
				return; // lat=0.0; lon=0.0

			if(cnt_comma==3) { // LAT
				while (gps[i] != ',')
					_lat[cnt_lat++] = gps[i++];
				i--; // here, it can be error TODO
			}

			if(cnt_comma==5) { // LON
				while (gps[i] != ',')
					_lng[cnt_lon++] = gps[i++];
				break;
			}
		}
		i++;
	}
	return;
}

void get_gps_timestamp(uint8_t *gps,  uint8_t *_date, uint8_t *_time)
{
	uint8_t i=0, cnt_comma=0, cnt_date=0, cnt_time=0;

	while(i < strlen((const char*)gps)) {
		if(gps[i]==',') {
			cnt_comma++;
			if(cnt_comma == 2) {
				if(gps[i+1] == 'V')
					return; // time, data default
				else
					break;
			}
		}
		i++;
	}

	i=0;
	cnt_comma=0;
	while (i < strlen((const char*)gps))
	{
		if (gps[i]==',') {
			cnt_comma++;
			i++;
			if (cnt_comma==1) { //time
				while (gps[i] != '.') {
					_time[cnt_time++] = gps[i++];
				}
				i=i+2;
			}
			if (cnt_comma==9) { //data
				while (gps[i] != ',') {
					_date[cnt_date++] = gps[i++];
				}
				break;
			}
		}
		i++;
	}
}

